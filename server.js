// module import
let express = require('express');
let mysql2 = require('mysql2');
let cookieParser = require('cookie-parser');

// setting
let server = express();
let dbPool = mysql2.createPool({
    host: 'localhost',
    user: 'jgh',
    password: '123456',
    database: 't_board',
    connectionLimit: 30,
    waitForConnections: true
});
server.set('view engine', 'ejs');   // 템플릿엔진의 종류설정
server.set('views', './templates'); // default views 폴더를 templates 폴더로 설정

server.use(express.static('./statics'));
server.use(express.urlencoded());
server.use(express.json());
server.use(cookieParser());

// middleware
function checkLogin(req, res, next) {
    if (req.cookies.SESSION_ID) {
        dbPool.query('SELECT * FROM sessions WHERE seq=?', [req.cookies.SESSION_ID], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            // 로그인 10분이내면 세션시간 10분연장
            let timeCheck = new Date() - result[0].s_date;
            if (timeCheck < 600000) {
                dbPool.query('UPDATE sessions SET s_date=? WHERE seq=?', [new Date(), req.cookies.SESSION_ID]);
                if (result.length > 0) {
                    let login_id = result[0].sess_data.split('=')[1];
                    req.body.member = login_id;
                }
                next();
                return;
            } else { // 로그인 10분 경과시 로그아웃처리
                res.redirect('/logout');
                return;
            }
        });
    } else {
        req["body"] = {member: null};
        next();
        return;
    }
}

// 페이징 변수
let paging = {};
paging.listCount = 10;                 // 화면에 표시될 게시글 수
paging.startListNum = 0;            // 화면에 표시될 게시글 시작번호
paging.endListNum = 0;              // 화면에 표시될 게시글 끝번호
paging.totalListCount = 0;          // 총 게시글 수
paging.totalPage = 0;               // 총 페이지 수
paging.pageCount = 5;                  // 화면에 표시될 페이지 수
paging.currentPage = 0;             // 현재 페이지 번호
paging.startPage = 0;               // 화면에 표시될 시작 페이지번호
paging.endPage = 0;                 // 화면에 표시될 끝 페이지번호

server.get('/', checkLogin, (req, res) => {
    let login_id = req.body.member;
    // 로그인 유무 확인
    if (login_id) { // 로그인 된경우 템플릿에 표시할 이름 가져오기
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            res.render('home', {login: result[0].name});
        });
    } else { // 로그인 10분경과시 로그아웃처리
        res.redirect('/logout');
    }
});
// 회원가입요청
server.get('/signup', (req, res) => {
    res.render('signup', {error: null, id: null, pw: null, re_pw: null, name: null, birth: null, email: null, phone: null});
});
// 로그인요청
server.get('/login', (req, res) => {
    res.render('login', {temp:null, id: null, pw: null});
});
// 로그아웃
server.get('/logout', (req, res) => {
    res.cookie('SESSION_ID', '');
    res.render('home', {login: null});
});

// 게시물 리스트 보기 요청
server.get('/list/:idx', checkLogin, (req, res, next) => {
    let login_id = req.body.member;
    dbPool.query('SELECT * FROM posts ORDER BY seq DESC', (err, posts) => {
        // 총 페이지 수
        paging.totalPage = Math.ceil(posts.length/paging.listCount);
        paging.currentPage = req.params.idx;

        paging.endPage = Math.ceil(paging.currentPage/paging.pageCount) * paging.pageCount;
        paging.startPage = paging.endPage - (paging.pageCount-1);
        // 총페이지 보다 화편에 표시될 끝 페이지 번호가 크면 총 페이지를 끝 페이지 번호에 대입한다
        if (paging.endPage >= paging.totalPage) {
            paging.endPage = paging.totalPage;
        }

        paging.endListNum = req.params.idx * paging.listCount;
        paging.startListNum = paging.endListNum - (paging.listCount - 1);

        // 로그인된경우 메뉴 (Home, List, 유저이름, Logout) 표시
        if (login_id) { // 로그인 된경우 템플릿에 표시할 이름 가져오기
            dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                } // 화면에 표시할 게시글 10개 가져오기
                dbPool.query('SELECT * FROM posts ORDER BY seq DESC LIMIT ?, ?',
                    [paging.startListNum - 1, paging.listCount], (err, results) => {
                        if (err) {
                            console.log(err);
                            return res.json(err);
                        }
                        res.render('list', {
                            login: result[0].name,    // 로그인된 이름
                            results: results,         // 게시글 10개
                            paging: paging            // 페이징 데이터
                        })
                    });
            });
        } else { // 로그인 안된 경우
            dbPool.query('SELECT * FROM posts ORDER BY seq DESC LIMIT ?, ?',
                [paging.startListNum-1, paging.listCount], (err, results) => {
                    if (err) {
                        console.log(err);
                        return res.json(err);
                    }
                    res.render('list', {
                        login: null,
                        results: results,
                        paging: paging
                    })
                });
        }
    });
});
// 게시물 상세보기 요청
server.get('/show/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
        if (err) {
            console.log(err);
            return res.json(err);
        } // 출력할 게시글 가져오기
        dbPool.query('SELECT * FROM posts WHERE seq=?', [req.params.idx], (err, list) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            let views = list[0].views + 1; // 조회수 증가 및 DB 저장
            dbPool.query('UPDATE posts SET views=? WHERE seq=?', [views, req.params.idx]);
            dbPool.query('SELECT * FROM posts WHERE seq=?', [req.params.idx], (err, post) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                // 게시글 작성자 이름가져오기
                dbPool.query('SELECT * FROM members WHERE seq=?', [post[0].members_seq], (err, member) => {
                    if (err) {
                        console.log(err);
                        return res.json(err);
                    }
                    dbPool.query('SELECT * FROM comments WHERE posts_seq=? ORDER BY createdAt',
                        [req.params.idx], (err, comment) => {
                            if (err) {
                                console.log(err);
                                return res.json(err);
                            }// 상세보기 글번호 템플릿에 넘기기
                            if (login_id) {  // 로그인 된 경우
                                res.render('show', {
                                    login: result[0].name,
                                    m_seq: result[0].seq,
                                    member: member[0].name,
                                    result: post,
                                    comment: comment,
                                    comment_seq: null
                                })
                            } else {  // 로그인 안된 경우
                                res.render('show', {
                                    login: null,
                                    m_seq: null,
                                    member: member[0].name,
                                    result: post,
                                    comment: comment,
                                    comment_seq: null
                                })
                            }
                        });
                });
            });
        });
    });
});
// 게시글 수정 요청
server.get('/edit/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('SELECT * FROM posts WHERE seq=?', [req.params.idx], (err, post) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                res.render('edit', {
                    login: result[0].name,
                    result: post,
                })
            });
        });
    } else {
        res.render('list', {login: null});
    }
});
// 새 게시글 작성 요청
server.get('/new', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            res.render('new', {
                login: result[0].name
            })
        });
    } else {
        res.render('list', {login: null});
    }
});
// 회원가입시 각 입력된 데이터 validation 체크 및 데이터 db 저장
server.post('/signup', (req, res) => {
    dbPool.query('SELECT * FROM members', (err, results) => {
        if (err) {
            console.log(err);
            return res.json(err);
        }
        if (!req.body.id) {
            //res.send('<meta charset="utf-8"><script>alert("아이디를 입력하세요!");location.href="/signup";</script>');
            res.render('signup', {
                error: "아이디를 입력하세요!",
                id: req.body.id,
                pw: req.body.pw,
                re_pw: req.body.re_pw,
                name: req.body.name,
                birth: req.body.birth,
                email: req.body.email,
                phone: req.body.phone
            });
            return;
        }
        if (!req.body.pw) {
            //res.send('<meta charset="utf-8"><script>alert("비밀번호를 입력하세요!");location.href="/signup";</script>');
            res.render('signup', {
                error: "비밀번호를 입력하세요!",
                id: req.body.id,
                pw: req.body.pw,
                re_pw: req.body.re_pw,
                name: req.body.name,
                birth: req.body.birth,
                email: req.body.email,
                phone: req.body.phone
            });
            return;
        }
        if (!req.body.name) {
            //res.send('<meta charset="utf-8"><script>alert("이름을 입력하세요!");location.href="/signup";</script>');
            res.render('signup', {
                error: "이름을 입력하세요!", id: req.body.id,
                pw: req.body.pw,
                re_pw: req.body.re_pw,
                name: req.body.name,
                birth: req.body.birth,
                email: req.body.email,
                phone: req.body.phone
            });
            return;
        }
        if (req.body.pw != req.body.re_pw) {
            //res.send('<meta charset="utf-8"><script>alert("비밀번호가 다릅니다!");location.href="/signup";</script>');
            res.render('signup', {
                error: "비밀번호가 다릅니다!",
                id: req.body.id,
                pw: req.body.pw,
                re_pw: req.body.re_pw,
                name: req.body.name,
                birth: req.body.birth,
                email: req.body.email,
                phone: req.body.phone
            });
            return;
        }
        for (let i = 0; i < results.length; i++) {
            if (results[i].id == req.body.id) {
                //res.send('<meta charset="utf-8"><script>alert("같은 아이디가 있습니다!");location.href="/signup";</script>');
                res.render('signup', {
                    error: "같은 아이디가 있습니다!", id: req.body.id,
                    pw: req.body.pw,
                    re_pw: req.body.re_pw,
                    name: req.body.name,
                    birth: req.body.birth,
                    email: req.body.email,
                    phone: req.body.phone
                });
                return;
            }
        };
        dbPool.query('INSERT INTO members SET id=?, pw=?, name=?, birth=?, email=?, phone=?',
            [req.body.id, req.body.pw, req.body.name, req.body.birth, req.body.email, req.body.phone],
            (err, result) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                //res.send('<meta charset="utf-8"><script>alert("회원가입이 완료되었습니다!");location.href="/login";</script>');
                res.render('login', {content: req.body.name +"님 회원가입이 완료되었습니다!"})
            });
    });
});
// 로그인시 각 항목 데이터 validation 체크 및 로그인 처리
server.post('/login', (req, res) => {
    dbPool.query('SELECT * FROM members WHERE id=? AND pw=?', [req.body.id, req.body.pw], (err, result) => {
        if (err) {
            console.log(err);
            return res.json(err);
        }
        if (result.length == 0) {
            res.render('login', {
                temp: "회원정보가 올바르지 않습니다!",
                id: req.body.id,
                pw: req.body.pw
            });
            return;
        }
        dbPool.query('INSERT INTO sessions SET sess_data=?, s_date=?', ['login=' + req.body.id, new Date()], (error, session) => {
            res.cookie('SESSION_ID', session.insertId);
            res.render('home', {login: result[0].name});
        });
    });
});
// 게시글 수정 data db 저장
server.post('/edit/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('UPDATE posts SET subject=?, content=?, update_at=? WHERE seq=?',
                [req.body.subject, req.body.content, new Date(), req.params.idx]);
            dbPool.query('SELECT * FROM posts WHERE seq=?', [req.params.idx], (err, post) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                dbPool.query('SELECT * FROM comments WHERE posts_seq=? ORDER BY createdAt',
                    [req.params.idx], (err, comment) => {
                        if (err) {
                            console.log(err);
                            return res.json(err);
                        }// 상세보기 글번호 템플릿에 넘기기*/
                        res.render('show', {
                            login: result[0].name,
                            m_seq: result[0].seq,
                            member: result[0].name,
                            result: post,
                            comment: comment,
                            comment_seq: null
                        })
                    });
            });
        });
    } else {
        res.render('list', {login: null});
    }
});
// 새 게시글 data db 저장
server.post('/new', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, member) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('INSERT INTO posts SET subject=?, content=?, create_at=?, members_seq=?',
                [req.body.subject, req.body.content, new Date, member[0].seq]);
            dbPool.query('SELECT * FROM posts WHERE members_seq=? ORDER BY create_at DESC LIMIT 0,1',
                [member[0].seq], (err, post) => {
                    if (err) {
                        console.log(err);
                        return res.json(err);
                    }
                    dbPool.query('SELECT * FROM comments WHERE posts_seq=? ORDER BY createdAt',
                        [req.params.idx], (err, comment) => {
                            if (err) {
                                console.log(err);
                                return res.json(err);
                            }// 상세보기 글번호 템플릿에 넘기기*/
                            res.render('show', {
                                login: member[0].name,
                                m_seq: member[0].seq,
                                member: member[0].name,
                                result: post,
                                comment: comment,
                                comment_seq: null
                            })
                        });
                });
        });
    } else {
        res.render('list', {login: null});
    }
});
// 게시글 삭제
server.post('/del/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('DELETE FROM posts WHERE seq=?', [req.params.idx]);
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('SELECT * FROM posts', (err, posts) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                dbPool.query('SELECT * FROM posts ORDER BY seq DESC LIMIT ?, ?',
                    [paging.startListNum-1, paging.listCount], (err, results) => {
                        if (err) {
                            console.log(err);
                            return res.json(err);
                        }
                        // 총 페이지 수
                        paging.totalPage = Math.ceil(posts.length/paging.listCount);
                        // 총페이지 보다 화편에 표시될 끝 페이지 번호가 크면 총 페이지를 끝 페이지 번호에 대입한다
                        if (paging.pageCount >= paging.totalPage) {
                            paging.endPage = paging.totalPage;
                        } else {
                            paging.endPage = paging.pageCount;
                        }
                        paging.startPage = 1;
                        console.dir(paging);
                        res.render('list', {
                            login: result[0].name,
                            results: results,
                            paging: paging
                        });
                    });
            });
        });
    } else {
        res.render('list', {login: null});
    }
});
// 댓글저장
server.post('/commentSave/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('INSERT INTO comments SET content=?, name=?, createdAt=?, posts_seq=?',
            [req.body.content, req.body.name, new Date, req.params.idx]);
        res.redirect('/show/'+req.params.idx);
    } else {
        res.render('list', {login: null});
    }
});
// 댓글삭제
server.get('/commentDel/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('SELECT * FROM comments WHERE seq=?', [req.params.idx], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('DELETE FROM comments WHERE seq=?', [req.params.idx]);
            res.redirect('/show/'+result[0].posts_seq);
        });
    } else {
        res.render('list', {login: null});
    }
});
// 댓글 수정후 화면 출력
server.get('/commentEdit/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('SELECT * FROM comments WHERE seq=?', [req.params.idx], (err, result) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('SELECT * FROM posts WHERE seq=?', [result[0].posts_seq], (err, post) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                dbPool.query('SELECT * FROM members WHERE seq=?', [post[0].members_seq], (err, member) => {
                    if (err) {
                        console.log(err);
                        return res.json(err);
                    }
                    dbPool.query('SELECT * FROM comments WHERE posts_seq=? ORDER BY createdAt',
                        [post[0].seq], (err, comment) => {
                            if (err) {
                                console.log(err);
                                return res.json(err);
                            }
                            res.render('show', {
                                login: result[0].name,
                                m_seq: post[0].members_seq,
                                member: member[0].name,
                                result: post,
                                comment: comment,
                                comment_seq: req.params.idx
                            });
                        });
                });
            });
        });
    } else {
        res.render('list', {login: null});
    }
});
// 댓글 수정시 DB 저장
server.post('/commentUpdate/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    if (login_id) {
        dbPool.query('UPDATE comments SET content=?, createdAt=? WHERE seq=?', [req.body.content, new Date(), req.params.idx]);
        dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, member) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            dbPool.query('SELECT * FROM comments WHERE seq=?', [req.params.idx], (err, comment) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                dbPool.query('SELECT * FROM posts WHERE seq=?', [comment[0].posts_seq], (err, post) => {
                    if (err) {
                        console.log(err);
                        return res.json(err);
                    }
                    dbPool.query('SELECT * FROM comments WHERE posts_seq=? ORDER BY createdAt', [post[0].seq], (err, comment) => {
                        if (err) {
                            console.log(err);
                            return res.json(err);
                        }
                        res.render('show', {
                            login: member[0].name,
                            m_seq: post[0].members_seq,
                            member: member[0].name,
                            result: post,
                            comment: comment,
                            comment_seq: null
                        })
                    });
                });
            });
        });
    } else {
        res.render('list', {login: null});
    }
});
// 좋아요 처리
server.get('/likes/:idx', checkLogin, (req, res) => {
    let login_id = req.body.member;
    dbPool.query('SELECT * FROM members WHERE id=?', [login_id], (err, result) => {
        if (err) {
            console.log(err);
            return res.json(err);
        }
        dbPool.query('SELECT * FROM posts WHERE seq=?', [req.params.idx], (err, like) => {
            if (err) {
                console.log(err);
                return res.json(err);
            }
            let likes = like[0].likes + 1;
            dbPool.query('UPDATE posts SET likes=? WHERE seq=?', [likes, req.params.idx]);
            dbPool.query('SELECT * FROM posts WHERE seq=?', [req.params.idx], (err, post) => {
                if (err) {
                    console.log(err);
                    return res.json(err);
                }
                // 게시글 작성자 이름가져오기
                dbPool.query('SELECT * FROM members WHERE seq=?', [post[0].members_seq], (err, member) => {
                    if (err) {
                        console.log(err);
                        return res.json(err);
                    }
                    dbPool.query('SELECT * FROM comments WHERE posts_seq=? ORDER BY createdAt',
                        [req.params.idx], (err, comment) => {
                            if (err) {
                                console.log(err);
                                return res.json(err);
                            }// 상세보기 글번호 템플릿에 넘기기
                            if (login_id) {  // 로그인 된 경우
                                res.render('show', {
                                    login: result[0].name,
                                    m_seq: result[0].seq,
                                    member: member[0].name,
                                    result: post,
                                    comment: comment,
                                    comment_seq: null
                                })
                            } else {  // 로그인 안된 경우
                                res.render('show', {
                                    login: null,
                                    m_seq: null,
                                    member: member[0].name,
                                    result: post,
                                    comment: comment,
                                    comment_seq: null
                                })
                            }
                        });
                });
            });
        });
    });
});

server.listen(3000, () => {
    console.log('서버의 3000번 포트가 준비되었습니다!');
});